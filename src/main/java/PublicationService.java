import model.Book;
import model.Magazine;
import model.Publication;
import repository.BookRepository;
import repository.MagazineRepository;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class PublicationService {
    private Map<String, Book> booksISBNMap;
    private Map<String, Magazine> magazinesISBNMap;
    private Map<String, List<Publication>> authorPublications;
    private Set<String> sortedBookTitles;
    private Set<String> sortedMagazineTitles;


    public void init(File booksFile, File magazinesFile) throws IOException {
        List<Book> books = new BookRepository(booksFile).getAll();
        List<Magazine> magazines = new MagazineRepository(magazinesFile).getAll();
        booksISBNMap = books.stream().collect(Collectors.toMap(Book::getISBN, c -> c));
        magazinesISBNMap = magazines.stream().collect(Collectors.toMap(Magazine::getISBN, c -> c));

        authorPublications = new HashMap<>();
        books.stream().forEach(b -> b.getAuthors().stream().forEach(a -> authorPublications.computeIfAbsent(a, empList -> new ArrayList<>()).add(b)));
        magazines.stream().forEach(b -> b.getAuthors().stream().forEach(a -> authorPublications.computeIfAbsent(a, empList -> new ArrayList<>()).add(b)));

        sortedBookTitles = new TreeSet<String>();
        books.stream().forEach(b -> sortedBookTitles.add(b.getTitle()));

        sortedMagazineTitles = new TreeSet<String>();
        magazines.stream().forEach(m -> sortedMagazineTitles.add(m.getTitle()));
    }

    public Book findBookByISBN(String s) {
        Book book = null;
        if (s != null) {
            return booksISBNMap.get(s);
        }
        return book;
    }

    public Magazine findMagazineByISBN(String s) {
        Magazine magazine = null;
        if (s != null) {
            return magazinesISBNMap.get(s);
        }
        return magazine;
    }

    public List<Publication> getPublicationsByAuthor(String authorEmail) {
        if (authorEmail != null) {
            return authorPublications.get(authorEmail);
        } else {
            return new ArrayList();
        }
    }

    public Collection<Book> getAllBooks() {
        return booksISBNMap.values();
    }

    public Collection<Magazine> getAllMagazines() {
        return magazinesISBNMap.values();
    }

    public Set<String> getSortedBookTitles() {
        return sortedBookTitles;
    }

    public Set<String> getSortedMagazineTitles() {
        return sortedMagazineTitles;
    }
}
