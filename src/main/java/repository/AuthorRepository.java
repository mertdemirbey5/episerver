package repository;

import model.Author;

import java.io.File;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthorRepository extends FileRepository<Author> {
    private static final String splitter = ";";

    public AuthorRepository(File data) {
        super(data);
    }

    public Author parse(String s) {
        Author author = null;
        if (s != null) {
            String[] authorInfo = s.split(splitter);
            if (authorInfo.length == 3 && Arrays.stream(authorInfo).allMatch(a -> !"".equals(a)) && isValidEmail(authorInfo[0])) {
                author = new Author(authorInfo[0], authorInfo[1], authorInfo[2]);
            }
        }
        return author;
    }

    private boolean isValidEmail(String emailAddress) {
        String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(emailAddress);
        return matcher.matches();
    }
}
