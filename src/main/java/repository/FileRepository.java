package repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class FileRepository<T> {
    private File data;

    public FileRepository(File data) {
        this.data = data;
    }

    public List<T> getAll() throws IOException {
        try (Stream<String> lines = Files.lines(data.toPath())) {
            return lines.skip(1)
                    .map(line -> parse(line))
                    .collect(Collectors.toList());
        }
    }

    public abstract T parse(String line);
}
