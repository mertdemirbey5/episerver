package repository;

import model.Book;

import java.io.File;
import java.util.Arrays;

public class BookRepository extends FileRepository<Book> {

    private static final String SPLITTER = ";";
    private static final String SUB_SPLITTER = ",";

    public BookRepository(File data) {
        super(data);
    }

    @Override
    public Book parse(String line) {
        Book book = null;
        if(line != null){
            String[] bookInfo = line.split(SPLITTER);
            if(bookInfo.length == 4){
                book = new Book(bookInfo[0],bookInfo[1], Arrays.asList(bookInfo[2].split(SUB_SPLITTER)), bookInfo[3]);
            }
        }
        return book;
    }
}
