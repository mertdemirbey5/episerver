package repository;

import model.Magazine;

import java.io.File;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.logging.Logger;

public class MagazineRepository extends FileRepository<Magazine> {
    private static Logger logger = Logger.getLogger( MagazineRepository.class.getName() );
    private static final String SPLITTER = ";";
    private static final String SUB_SPLITTER = ",";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public MagazineRepository(File data) {
        super(data);
    }

    @Override
    public Magazine parse(String line) {
        Magazine magazine = null;
        if(line != null){
           String[] magazineInfo = line.split(SPLITTER);
           if(magazineInfo.length == 4){
               magazine = new Magazine(magazineInfo[0], magazineInfo[1],
                       Arrays.asList(magazineInfo[2].split(SUB_SPLITTER)), parseDate(magazineInfo[3]));
           }
        }
        return magazine;
    }

    private static LocalDate parseDate(String dateString){
        LocalDate date = null;
        try{
            date = LocalDate.parse(dateString, DATE_FORMATTER);
        }catch (DateTimeException exc){
            logger.warning("date format excepteption = "+ dateString);
        }
        return date;
    }
}
