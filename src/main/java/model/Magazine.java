package model;

import java.time.LocalDate;
import java.util.List;

public class Magazine extends Publication {
    private LocalDate publicationDate;

    public Magazine(String title, String ISBN, List<String> authors, LocalDate publicationDate) {
        super(title, ISBN, authors);
        this.publicationDate = publicationDate;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

}
