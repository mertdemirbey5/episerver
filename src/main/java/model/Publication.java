package model;

import java.util.List;

public class Publication {
    private String title;
    private String ISBN;
    private List<String> authors;

    public Publication(String title, String ISBN, List<String> authors) {
        this.title = title;
        this.ISBN = ISBN;
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public String getISBN() {
        return ISBN;
    }

    public List<String> getAuthors() {
        return authors;
    }
}
