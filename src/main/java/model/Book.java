package model;

import java.util.List;

public class Book extends Publication {
    private String shortDescription;

    public Book(String title, String ISBN, List<String> authors, String shortDescription) {
        super(title, ISBN, authors);
        this.shortDescription = shortDescription;
    }

    public String getShortDescription() {
        return shortDescription;
    }

}
