import model.Book;
import model.Magazine;
import model.Publication;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class PublicationServiceTest {

    private static PublicationService publicationService;

    @BeforeAll
    static void initAll() throws IOException {
        publicationService = new PublicationService();
        File booksFile = new File(publicationService.getClass().getResource("/data/buecher.csv").getFile());
        File magazinesFile = new File(publicationService.getClass().getResource("/data/zeitschriften.csv").getFile());
        publicationService.init(booksFile, magazinesFile);
    }

    @Test
    public void testBoookSearchByISBN() {
        Book book = publicationService.findBookByISBN("3214-5698-7412");
        assertNotNull(book);
        assertEquals("Das Piraten-Kochbuch beweist, dass die Seeräuberkapitäne zwar gefürchtete Haudegen waren, " +
                "andererseits aber manches Mal mit gehobenenem Geschmacksempfinden ausgestattet. ... Kurzum, ein ideales " +
                "Buch, um maritime Events kulinarisch zu umrahmen.", book.getShortDescription());
        assertEquals("Das Piratenkochbuch. Ein Spezialitätenkochbuch mit den 150 leckersten Rezepten ", book.getTitle());
        assertEquals("3214-5698-7412", book.getISBN());
        assertEquals("pr-rabe@optivo.de", book.getAuthors().get(0));
    }

    @Test
    public void testMagazineSearchByISBN() {
        Magazine magazine = publicationService.findMagazineByISBN("2365-5632-7854");
        assertNotNull(magazine);
        assertEquals("Kochen für Genießer", magazine.getTitle());
        assertEquals("2365-5632-7854", magazine.getISBN());
        assertEquals(LocalDate.parse("01.05.2007", DateTimeFormatter.ofPattern("dd.MM.yyyy")), magazine.getPublicationDate());
        assertEquals("pr-lieblich@optivo.de", magazine.getAuthors().get(0));
        assertEquals("pr-walter@optivo.de", magazine.getAuthors().get(1));
    }

    @Test
    public void testBooksSortedByTitle() {
        Set<String> sortedMagazines = publicationService.getSortedBookTitles();
        List<String> sortedList = sortedMagazines.stream().collect(Collectors.toList());
        assertEquals(8, sortedList.size());
        assertEquals("Das Perfekte Dinner. Die besten Rezepte", sortedList.get(0));
        assertEquals("Das Piratenkochbuch. Ein Spezialitätenkochbuch mit den 150 leckersten Rezepten ", sortedList.get(1));
        assertEquals("Das große GU-Kochbuch Kochen für Kinder", sortedList.get(2));
        assertEquals("Genial italienisch", sortedList.get(3));
        assertEquals("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit großem Backteil", sortedList.get(4));
        assertEquals("O'Reillys Kochbuch für Geeks", sortedList.get(5));
        assertEquals("Schlank im Schlaf ", sortedList.get(6));
        assertEquals("Schuhbecks Kochschule. Kochen lernen mit Alfons Schuhbeck ", sortedList.get(7));
    }

    @Test
    public void testMagazinesSortedByTitle() {
        Set<String> sortedMagazines = publicationService.getSortedMagazineTitles();
        List<String> sortedList = sortedMagazines.stream().collect(Collectors.toList());
        assertEquals(6, sortedList.size());
        assertEquals("Der Weinkenner", sortedList.get(0));
        assertEquals("Gourmet", sortedList.get(1));
        assertEquals("Kochen für Genießer", sortedList.get(2));
        assertEquals("Meine Familie und ich", sortedList.get(3));
        assertEquals("Schöner kochen", sortedList.get(4));
        assertEquals("Vinum", sortedList.get(5));
    }

    @Test
    public void testGetAllMagazines() {
        Collection<Magazine> col = publicationService.getAllMagazines();
        assertEquals(6, col.size());
    }

    @Test
    public void testGetAllBooks() {
        Collection<Book> col = publicationService.getAllBooks();
        assertEquals(8, col.size());
    }

    @Test
    public void testFindPublicationByAuthor() {
        List<Publication> publications = publicationService.getPublicationsByAuthor("pr-gustafsson@optivo.de");
        assertEquals(2, publications.size());
        assertEquals("Schlank im Schlaf ", publications.get(0).getTitle());
        assertTrue(publications.get(0) instanceof Book);
        assertEquals("Vinum", publications.get(1).getTitle());
        assertTrue(publications.get(1) instanceof Magazine);
    }
}
