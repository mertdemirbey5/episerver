package repository;

import model.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class BookRepositoryTest {

    @Test
    public void testParseWithValidString() {
        BookRepository repository = new BookRepository(null);
        Book book = repository.parse("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit großem Backteil;" +
                "5554-5545-4518;pr-walter@optivo.de,adam@optivo.de;Auf der Suche nach einem Basiskochbuch steht man heutzutage vor " +
                "einer Fülle von Alternativen. Es fällt schwer, daraus die für sich passende Mixtur aus Grundlagenwerk " +
                "und Rezeptesammlung zu finden. Man sollte sich darüber im Klaren sein, welchen Schwerpunkt man setzen " +
                "möchte oder von welchen Koch- und Backkenntnissen man bereits ausgehen kann.");
        assertNotNull(book);
        assertEquals("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit großem Backteil", book.getTitle());
        assertEquals("5554-5545-4518", book.getISBN());
        assertEquals("pr-walter@optivo.de", book.getAuthors().get(0));
        assertEquals("adam@optivo.de", book.getAuthors().get(1));
        assertEquals("Auf der Suche nach einem Basiskochbuch steht man heutzutage vor einer Fülle von " +
                "Alternativen. Es fällt schwer, daraus die für sich passende Mixtur aus Grundlagenwerk und " +
                "Rezeptesammlung zu finden. Man sollte sich darüber im Klaren sein, welchen Schwerpunkt man setzen " +
                "möchte oder von welchen Koch- und Backkenntnissen man bereits ausgehen kann.", book.getShortDescription());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "title;323-2323;pr-walter@optivo.de",
            "title;323-2323;pr-walter@optivo.de;description;extra", ";;;", "; ; ;"})
    public void testParseWithNotValidString(String parseString) {
        BookRepository repository = new BookRepository(null);
        Book book = repository.parse(parseString);
        assertNull(book);
    }
}
