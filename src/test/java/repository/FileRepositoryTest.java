package repository;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileRepositoryTest {

    class Product {
        String name;
        String description;
    }

    @Test
    public void testDefault() throws IOException {

        FileRepository repository = new FileRepository<Product>(new File(this.getClass().getResource("/filerepositorytest.txt").getFile())) {
            @Override
            public Product parse(String line) {
                String[] prodAttr = line.split(";");
                Product product = new Product();
                product.name = prodAttr[0];
                product.description = prodAttr[1];
                return product;
            }
        };

        List<Product> products = repository.getAll();
        assertEquals(2, products.size());
        assertEquals("sony tv", products.get(0).name);
        assertEquals("sony oled tv", products.get(0).description);
        assertEquals("bmw 330", products.get(1).name);
        assertEquals("bmw 330 electric car", products.get(1).description);
    }


}
