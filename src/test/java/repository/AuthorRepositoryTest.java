package repository;

import model.Author;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class AuthorRepositoryTest {

    @Test
    public void testParseWithValidString() {
        AuthorRepository repo = new AuthorRepository(null);
        Author author = repo.parse("pr-walter@optivo.de;Paul;Walter");
        assertEquals("Paul", author.getFirstName());
        assertEquals("Walter", author.getLastName());
        assertEquals("pr-walter@optivo.de", author.getEmailAddress());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "pr-walter@optivo.de", "pr-walter@optivo.de;Paul", ";;;", "; ; ;"})
    public void testParseWithNotValidString(String parseString) {
        AuthorRepository repo = new AuthorRepository(null);
        Author author = repo.parse(parseString);
        assertNull(author);
    }

    @ParameterizedTest
    @ValueSource(strings = {"pr-walter@;Paul;Walter", "pr-walter;Paul;Walter"})
    public void testVParseWithNotValidEmailString(String parseString) {
        AuthorRepository repo = new AuthorRepository(null);
        Author author = repo.parse(parseString);
        assertNull(author);
    }
}
