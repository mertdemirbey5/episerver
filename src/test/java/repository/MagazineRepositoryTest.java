package repository;

import model.Magazine;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class MagazineRepositoryTest {


    @Test
    public void testParseWithValidString() {
        MagazineRepository repository = new MagazineRepository(null);
        Magazine magazine = repository.parse("Schöner kochen;5454-5587-3210;pr-walter@optivo.de,adam@optivo.de;21.05.2006");
        assertNotNull(magazine);
        assertEquals("Schöner kochen", magazine.getTitle());
        assertEquals("5454-5587-3210", magazine.getISBN());
        assertEquals("pr-walter@optivo.de", magazine.getAuthors().get(0));
        assertEquals("adam@optivo.de", magazine.getAuthors().get(1));
        assertEquals(LocalDate.parse("21.05.2006", DateTimeFormatter.ofPattern("dd.MM.yyyy")), magazine.getPublicationDate());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "Schöner kochen;5454-5587-3210;pr-walter@optivo.de",
            "Schöner kochen;5454-5587-3210;pr-walter@optivo.de,adam@optivo.de;21.05.2006;extra", ";;;", "; ; ;"})
    public void testParseWithNotValidString(String parseString) {
        MagazineRepository repository = new MagazineRepository(null);
        Magazine magazine = repository.parse(parseString);
        assertNull(magazine);
    }

    @Test
    public void testParseWithNotValidDateString() {
        MagazineRepository repository = new MagazineRepository(null);
        Magazine magazine = repository.parse("Schöner kochen;5454-5587-3210;pr-walter@optivo.de,adam@optivo.de;21.14.2006");
        assertNotNull(magazine);
        assertEquals("Schöner kochen", magazine.getTitle());
        assertEquals("5454-5587-3210", magazine.getISBN());
        assertEquals("pr-walter@optivo.de", magazine.getAuthors().get(0));
        assertEquals("adam@optivo.de", magazine.getAuthors().get(1));
        assertEquals(null, magazine.getPublicationDate());
    }

}
